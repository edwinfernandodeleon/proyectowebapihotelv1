﻿var ModelReservacion = function () {
    var instancia = this;
    var reservacionUri = 'api/Reservaciones/';
    var clienteUri = 'api/Clientes/';
    var hotelUri = 'api/Hoteles/';
    var tipoHabitacionUri = 'api/TipoHabitaciones/';

    instancia.reservaciones = ko.observableArray([]);
    instancia.clientes = ko.observableArray([]);
    instancia.hoteles = ko.observableArray([]);
    instancia.tipoHabitaciones = ko.observableArray([]);

    instancia.error = ko.observable();
    instancia.reservacionCargada = ko.observable();

    instancia.reservacionNueva = {
        NombreCliente: ko.observable(),
        Hotel: ko.observable(),
        CodigoHabitacion: ko.observable(),
        //Estado: ko.observable(),
        TipoHabitacion: ko.observable(),
        PrecioPorDia: ko.observable(),
        FechaIngreso: ko.observable(),
        FechaEgreso: ko.observable(),
        SubTotal: ko.observable(),
    }

    instancia.getAllReservaciones = function () {
        ajaxHelper(reservacionUri, 'GET')
        .done(function (data) {
            instancia.reservaciones(data);
        });
    }

    instancia.getAllClientes = function () {
        ajaxHelper(clienteUri, 'GET')
        .done(function (data) {
            instancia.clientes(data);
        });
    }

    instancia.getAllHoteles = function () {
        ajaxHelper(hotelUri, 'GET')
        .done(function (data) {
            instancia.hoteles(data);
        });
    }

    instancia.getAllTipoHabitacion = function () {
        ajaxHelper(tipoHabitacionUri, 'GET')
        .done(function (data) {
            instancia.tipoHabitaciones(data);
        });
    }

    /*instancia.estados = function () {
    var a = instancia.getAllReservaciones();
        if(a == true){
            $("#estado").attr('checked', true);
        }
        else if(a == 0)
        {
            $("#estado").attr('checked', false);
        }
    }*/

    instancia.agregar = function () {
        var reservacion = {
            IdCliente: instancia.reservacionNueva.NombreCliente().IdCliente,
            IdHotel: instancia.reservacionNueva.Hotel().IdHotel,
            CodigoHabitacion: instancia.reservacionNueva.CodigoHabitacion(),
            //Estado: instancia.reservacionNueva.Estado(),
            IdTipoHabitacion: instancia.reservacionNueva.TipoHabitacion().IdTipoHabitacion,
            PrecioPorDia: instancia.reservacionNueva.PrecioPorDia(),
            FechaIngreso: instancia.reservacionNueva.FechaIngreso(),
            FechaEgreso: instancia.reservacionNueva.FechaEgreso(),
            SubTotal: instancia.reservacionNueva.SubTotal()
        }

        ajaxHelper(reservacionUri, 'POST', reservacion)
        .done(function (data) {
            instancia.getAllReservaciones();
            $("#modalAgregarReservacion").modal('hide');
        });
    }

    instancia.editar = function () {
        var reservacion = {
            IdReservacion: instancia.reservacionCargada().IdReservacion,
            IdCliente: instancia.reservacionCargada().NombreCliente.IdCliente,
            IdHotel: instancia.reservacionCargada().Hotel.IdHotel,
            CodigoHabitacion: instancia.reservacionCargada().CodigoHabitacion,
            //Estado: instancia.reservacionNueva.Estado(),
            IdTipoHabitacion: instancia.reservacionCargada().TipoHabitacion.IdTipoHabitacion,
            PrecioPorDia: instancia.reservacionCargada().PrecioPorDia,
            FechaIngreso: instancia.reservacionCargada().FechaIngreso,
            FechaEgreso: instancia.reservacionCargada().FechaEgreso,
            SubTotal: instancia.reservacionCargada().SubTotal
        }
        var uriEditar = reservacionUri + reservacion.IdReservacion;
        ajaxHelper(uriEditar, 'PUT', reservacion)
        .done(function (data) {
            instancia.getAllReservaciones();
            $("#modalEditarReservacion").modal('hide');
        });
    }

    instancia.eliminar = function (item) {
        var id = item.IdReservacion;
        var uri = reservacionUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            instancia.getAllReservaciones();
        });
    }

    instancia.cargar = function (item) {
        instancia.reservacionCargada(item);
    }

    function ajaxHelper(uri, method, data) {
        instancia.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            instancia.error(errorThrown);
        });
    }

    instancia.getAllReservaciones();
    instancia.getAllClientes();
    instancia.getAllHoteles();
    instancia.getAllTipoHabitacion();
}

$(document).ready(function () {
    var modelReservaciones = new ModelReservacion();
    ko.applyBindings(modelReservaciones);

    
});