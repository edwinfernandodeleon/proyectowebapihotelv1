﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoWebApiHotelV1.Models
{
    public class Facturas
    {
        [Key]
        public int IdFactura { get; set; }

        [Required]
        public DateTime FechaHora { get; set; }
        
        [Required]
        public string Nit { get; set; }

        [Required]
        public string Nombre { get; set; }

        public int IdReservacion { get; set; }
        public virtual Reservaciones IdGastosReservacion { get; set; }

        [Required]
        public decimal SubTotalReservacion { get; set; }

        public int IdServicio { get; set; }
        public virtual Servicios IdGastosServicios { get; set; }

        public decimal SubTotalServicios { get; set; }

        [Required]
        public decimal Total { get; set; }
    }
}