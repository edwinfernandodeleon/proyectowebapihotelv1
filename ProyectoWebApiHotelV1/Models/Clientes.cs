﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Clientes
    {
        [Key]
        public int IdCliente { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Nit { get; set; }

        
        public string Direccion { get; set; }

        
        public string Telefono { get; set; }

        
        public string Correo { get; set; }

        public int IdUsuario { get; set; }
        public virtual Usuarios CuentaUsuario { get; set; }

    }
}
 