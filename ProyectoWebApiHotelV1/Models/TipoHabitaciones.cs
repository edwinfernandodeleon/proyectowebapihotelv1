﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class TipoHabitaciones
    {
        [Key]
        public int IdTipoHabitacion { get; set; }

        [Required]
        public string Tipo { get; set; }

        [Required]
        public decimal Precio { get; set; }
    }
}