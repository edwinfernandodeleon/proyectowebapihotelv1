﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Eventos
    {
        [Key]
        public int IdEvento { get; set; }

        [Required]
        public string NombreEvento { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime FechaEvento { get; set; }

        public int IdHotel  { get; set; }
        public virtual Hoteles Hotel { get; set; }
    }
}