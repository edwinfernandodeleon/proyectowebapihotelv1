﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Privilegios
    {
        [Key]
        public int IdPrivilegio { get; set; }

        [Required]
        public string Privilegio { get; set; }
    }
}