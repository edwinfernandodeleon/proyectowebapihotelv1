﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Hoteles
    {
        [Key]
        public int IdHotel { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string CantidadHabitaciones { get; set; }

        [Required]
        public string RangoHabitaciones { get; set; }

        [Required]
        public string Direccion { get; set; }

        [Required]
        public string Telefono { get; set; }

        public int IdUsuario { get; set; }
        public virtual Usuarios UsuarioAdmin { get; set; }
    }
}