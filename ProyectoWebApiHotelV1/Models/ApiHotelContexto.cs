﻿namespace ProyectoWebApiHotelV1.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ApiHotelContexto : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'ApiHotelContexto' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'ProyectoWebApiHotelV1.Models.ApiHotelContexto' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'ApiHotelContexto'  en el archivo de configuración de la aplicación.
        public ApiHotelContexto()
            : base("name=ApiHotelContexto")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public DbSet<Privilegios> Privilegio { get; set; }
        public DbSet<Usuarios> Usuario { get; set; }
        public DbSet<TipoHabitaciones> TipoHabitacion { get; set; }
        public DbSet<Hoteles> Hotel { get; set; }
        public DbSet<Eventos> Evento { get; set; }
        public DbSet<Clientes> Cliente { get; set; }
        public DbSet<Reservaciones> Reservacion { get; set; }
        public DbSet<Servicios> Servicio { get; set; }
        public DbSet<Facturas> Factura { get; set; }
    }

    //////codigo de login

    /*protected override void OnModelCreating (DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Usuarios>()
            .Property(e => e.Usuario)
            .IsUnicode(false);

        modelBuilder.Entity<Usuarios>()
            .Property(e => e.Contrasena)
            .IsUnicode(false);
    }*/

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}