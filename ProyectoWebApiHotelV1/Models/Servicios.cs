﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Servicios
    {
        [Key]
        public int IdServicio { get; set; }

        [Required]
        public DateTime FechaHora { get; set; }

        [Required]
        public string Descripcion { get; set; }

        [Required]
        public decimal Precio { get; set; } 

        public int IdReservacion { get; set; }
        public Reservaciones NumeroReservacion{ get; set; }
    }
}