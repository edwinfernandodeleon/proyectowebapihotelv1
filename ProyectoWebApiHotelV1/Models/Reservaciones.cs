﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoWebApiHotelV1.Models
{
    public class Reservaciones
    {
        [Key]
        public int IdReservacion { get; set; }

        public int IdCliente { get; set; }
        public virtual Clientes NombreCliente { get; set; }

        public int IdHotel{ get; set; }
        public virtual Hoteles Hotel { get; set; }

        [Required]
        public string CodigoHabitacion { get; set; }

        [Required]
        public bool Estado { get; set; }

        public int IdTipoHabitacion {get;set;}
        public virtual TipoHabitaciones TipoHabitacion { get; set; }

        [Required]
        public decimal PrecioPorDia { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime FechaIngreso { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime FechaEgreso { get; set; }

        [Required]
        public decimal SubTotal { get; set; }
    }
}