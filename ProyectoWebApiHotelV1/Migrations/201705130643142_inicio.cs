namespace ProyectoWebApiHotelV1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        IdCliente = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Nit = c.String(nullable: false),
                        Direccion = c.String(),
                        Telefono = c.String(),
                        Correo = c.String(),
                        IdUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdCliente)
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: false)
                .Index(t => t.IdUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Usuario = c.String(nullable: false),
                        Contrasena = c.String(nullable: false),
                        IdPrivilegio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdUsuario)
                .ForeignKey("dbo.Privilegios", t => t.IdPrivilegio, cascadeDelete: false)
                .Index(t => t.IdPrivilegio);
            
            CreateTable(
                "dbo.Privilegios",
                c => new
                    {
                        IdPrivilegio = c.Int(nullable: false, identity: true),
                        Privilegio = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdPrivilegio);
            
            CreateTable(
                "dbo.Eventos",
                c => new
                    {
                        IdEvento = c.Int(nullable: false, identity: true),
                        NombreEvento = c.String(nullable: false),
                        FechaEvento = c.DateTime(nullable: false),
                        IdHotel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdEvento)
                .ForeignKey("dbo.Hoteles", t => t.IdHotel, cascadeDelete: false)
                .Index(t => t.IdHotel);
            
            CreateTable(
                "dbo.Hoteles",
                c => new
                    {
                        IdHotel = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        CantidadHabitaciones = c.String(nullable: false),
                        RangoHabitaciones = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        IdUsuario = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdHotel)
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: false)
                .Index(t => t.IdUsuario);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        IdFactura = c.Int(nullable: false, identity: true),
                        FechaHora = c.DateTime(nullable: false),
                        Nit = c.String(nullable: false),
                        Nombre = c.String(nullable: false),
                        IdReservacion = c.Int(nullable: false),
                        SubTotalReservacion = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdServicio = c.Int(nullable: false),
                        SubTotalServicios = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.IdFactura)
                .ForeignKey("dbo.Reservaciones", t => t.IdReservacion, cascadeDelete: true)
                .ForeignKey("dbo.Servicios", t => t.IdServicio, cascadeDelete: true)
                .Index(t => t.IdReservacion)
                .Index(t => t.IdServicio);
            
            CreateTable(
                "dbo.Reservaciones",
                c => new
                    {
                        IdReservacion = c.Int(nullable: false, identity: true),
                        IdCliente = c.Int(nullable: false),
                        IdHotel = c.Int(nullable: false),
                        CodigoHabitacion = c.String(nullable: false),
                        IdTipoHabitacion = c.Int(nullable: false),
                        PrecioPorDia = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FechaIngreso = c.DateTime(nullable: false),
                        FechaEgreso = c.DateTime(nullable: false),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.IdReservacion)
                .ForeignKey("dbo.Hoteles", t => t.IdHotel, cascadeDelete: false)
                .ForeignKey("dbo.Clientes", t => t.IdCliente, cascadeDelete: false)
                .ForeignKey("dbo.TipoHabitaciones", t => t.IdTipoHabitacion, cascadeDelete: false)
                .Index(t => t.IdCliente)
                .Index(t => t.IdHotel)
                .Index(t => t.IdTipoHabitacion);
            
            CreateTable(
                "dbo.TipoHabitaciones",
                c => new
                    {
                        IdTipoHabitacion = c.Int(nullable: false, identity: true),
                        Tipo = c.String(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.IdTipoHabitacion);
            
            CreateTable(
                "dbo.Servicios",
                c => new
                    {
                        IdServicio = c.Int(nullable: false, identity: true),
                        FechaHora = c.DateTime(nullable: false),
                        Descripcion = c.String(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdReservacion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdServicio)
                .ForeignKey("dbo.Reservaciones", t => t.IdReservacion, cascadeDelete: false)
                .Index(t => t.IdReservacion);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Facturas", "IdServicio", "dbo.Servicios");
            DropForeignKey("dbo.Servicios", "IdReservacion", "dbo.Reservaciones");
            DropForeignKey("dbo.Facturas", "IdReservacion", "dbo.Reservaciones");
            DropForeignKey("dbo.Reservaciones", "IdTipoHabitacion", "dbo.TipoHabitaciones");
            DropForeignKey("dbo.Reservaciones", "IdCliente", "dbo.Clientes");
            DropForeignKey("dbo.Reservaciones", "IdHotel", "dbo.Hoteles");
            DropForeignKey("dbo.Eventos", "IdHotel", "dbo.Hoteles");
            DropForeignKey("dbo.Hoteles", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Clientes", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "IdPrivilegio", "dbo.Privilegios");
            DropIndex("dbo.Servicios", new[] { "IdReservacion" });
            DropIndex("dbo.Reservaciones", new[] { "IdTipoHabitacion" });
            DropIndex("dbo.Reservaciones", new[] { "IdHotel" });
            DropIndex("dbo.Reservaciones", new[] { "IdCliente" });
            DropIndex("dbo.Facturas", new[] { "IdServicio" });
            DropIndex("dbo.Facturas", new[] { "IdReservacion" });
            DropIndex("dbo.Hoteles", new[] { "IdUsuario" });
            DropIndex("dbo.Eventos", new[] { "IdHotel" });
            DropIndex("dbo.Usuarios", new[] { "IdPrivilegio" });
            DropIndex("dbo.Clientes", new[] { "IdUsuario" });
            DropTable("dbo.Servicios");
            DropTable("dbo.TipoHabitaciones");
            DropTable("dbo.Reservaciones");
            DropTable("dbo.Facturas");
            DropTable("dbo.Hoteles");
            DropTable("dbo.Eventos");
            DropTable("dbo.Privilegios");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Clientes");
        }
    }
}
