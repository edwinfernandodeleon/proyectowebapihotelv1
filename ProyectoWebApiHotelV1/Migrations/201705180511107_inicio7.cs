namespace ProyectoWebApiHotelV1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservaciones", "Estado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reservaciones", "Estado");
        }
    }
}
