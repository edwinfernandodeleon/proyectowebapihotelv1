﻿using System.Web.Mvc;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Inicio";

            return View();
        }

    }
}
