﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class HotelController : Controller
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: Hotel
        public async Task<ActionResult> Index()
        {
            var hotel = db.Hotel.Include(h => h.UsuarioAdmin);
            return View(await hotel.ToListAsync());
        }

        // GET: Hotel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            if (hoteles == null)
            {
                return HttpNotFound();
            }
            return View(hoteles);
        }

        // GET: Hotel/Create
        public ActionResult Create()
        {
            ViewBag.IdUsuario = new SelectList(db.Usuario, "IdUsuario", "Usuario");
            return View();
        }

        // POST: Hotel/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdHotel,Nombre,CantidadHabitaciones,RangoHabitaciones,Direccion,Telefono,IdUsuario")] Hoteles hoteles)
        {
            if (ModelState.IsValid)
            {
                db.Hotel.Add(hoteles);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdUsuario = new SelectList(db.Usuario, "IdUsuario", "Usuario", hoteles.IdUsuario);
            return View(hoteles);
        }

        // GET: Hotel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            if (hoteles == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdUsuario = new SelectList(db.Usuario, "IdUsuario", "Usuario", hoteles.IdUsuario);
            return View(hoteles);
        }

        // POST: Hotel/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdHotel,Nombre,CantidadHabitaciones,RangoHabitaciones,Direccion,Telefono,IdUsuario")] Hoteles hoteles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoteles).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdUsuario = new SelectList(db.Usuario, "IdUsuario", "Usuario", hoteles.IdUsuario);
            return View(hoteles);
        }

        // GET: Hotel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            if (hoteles == null)
            {
                return HttpNotFound();
            }
            return View(hoteles);
        }

        // POST: Hotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            db.Hotel.Remove(hoteles);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
