﻿using ProyectoWebApiHotelV1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class SesionController : Controller
    {
        // GET: Sesion
        public ActionResult Index()
        {
            return View();
        }

        //----------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Usuarios objUser)
        {
            if (ModelState.IsValid)
            {
                using (ApiHotelContexto db = new ApiHotelContexto())
                {
                    var obj = db.Usuario.Where(a => a.Usuario.Equals(objUser.Usuario) 
                        && a.Contrasena.Equals(objUser.Contrasena)
                        ).FirstOrDefault();


                    if (obj != null && obj.Privilegio.Privilegio == "Administrador")
                    {

                        Session["IdUsuario"] = obj.IdUsuario.ToString();
                        Session["Usuario"] = obj.Usuario.ToString();
                        Session["Privilegio"] = obj.Privilegio.Privilegio.ToString();
                        return RedirectToAction("../Administrador");
                    }
                    else if (obj != null && obj.Privilegio.Privilegio == "Cliente")
                    { 
                        Session["IdUsuario"] = obj.IdUsuario.ToString();
                        Session["Usuario"] = obj.Usuario.ToString();
                        Session["Privilegio"] = obj.Privilegio.Privilegio.ToString();
                        return RedirectToAction("../Clientela");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                    
                }
            }
            return View(objUser);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("../Home/Index");
        }

        public ActionResult Administrador()
        {
            if (Session["IdUsuario"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        //------------------
    }
}