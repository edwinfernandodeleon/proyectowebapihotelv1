﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers.api
{
    public class HotelesController : ApiController
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: api/Hoteles
        public IQueryable<Hoteles> GetHotel()
        {
            return db.Hotel;
        }

        // GET: api/Hoteles/5
        [ResponseType(typeof(Hoteles))]
        public async Task<IHttpActionResult> GetHoteles(int id)
        {
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            if (hoteles == null)
            {
                return NotFound();
            }

            return Ok(hoteles);
        }

        // PUT: api/Hoteles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHoteles(int id, Hoteles hoteles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hoteles.IdHotel)
            {
                return BadRequest();
            }

            db.Entry(hoteles).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HotelesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hoteles
        [ResponseType(typeof(Hoteles))]
        public async Task<IHttpActionResult> PostHoteles(Hoteles hoteles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hotel.Add(hoteles);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = hoteles.IdHotel }, hoteles);
        }

        // DELETE: api/Hoteles/5
        [ResponseType(typeof(Hoteles))]
        public async Task<IHttpActionResult> DeleteHoteles(int id)
        {
            Hoteles hoteles = await db.Hotel.FindAsync(id);
            if (hoteles == null)
            {
                return NotFound();
            }

            db.Hotel.Remove(hoteles);
            await db.SaveChangesAsync();

            return Ok(hoteles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HotelesExists(int id)
        {
            return db.Hotel.Count(e => e.IdHotel == id) > 0;
        }
    }
}