﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers.api
{
    public class ReservacionesController : ApiController
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: api/Reservaciones
        public IQueryable<Reservaciones> GetReservacion()
        {
            return db.Reservacion;
        }

        // GET: api/Reservaciones/5
        [ResponseType(typeof(Reservaciones))]
        public async Task<IHttpActionResult> GetReservaciones(int id)
        {
            Reservaciones reservaciones = await db.Reservacion.FindAsync(id);
            if (reservaciones == null)
            {
                return NotFound();
            }

            return Ok(reservaciones);
        }

        // PUT: api/Reservaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReservaciones(int id, Reservaciones reservaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservaciones.IdReservacion)
            {
                return BadRequest();
            }

            db.Entry(reservaciones).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReservacionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reservaciones
        [ResponseType(typeof(Reservaciones))]
        public async Task<IHttpActionResult> PostReservaciones(Reservaciones reservaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reservacion.Add(reservaciones);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = reservaciones.IdReservacion }, reservaciones);
        }

        // DELETE: api/Reservaciones/5
        [ResponseType(typeof(Reservaciones))]
        public async Task<IHttpActionResult> DeleteReservaciones(int id)
        {
            Reservaciones reservaciones = await db.Reservacion.FindAsync(id);
            if (reservaciones == null)
            {
                return NotFound();
            }

            db.Reservacion.Remove(reservaciones);
            await db.SaveChangesAsync();

            return Ok(reservaciones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReservacionesExists(int id)
        {
            return db.Reservacion.Count(e => e.IdReservacion == id) > 0;
        }
    }
}