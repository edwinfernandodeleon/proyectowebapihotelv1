﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers.api
{
    public class PrivilegiosController : ApiController
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: api/Privilegios
        public IQueryable<Privilegios> GetPrivilegio()
        {
            return db.Privilegio;
        }

        // GET: api/Privilegios/5
        [ResponseType(typeof(Privilegios))]
        public async Task<IHttpActionResult> GetPrivilegios(int id)
        {
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            if (privilegios == null)
            {
                return NotFound();
            }

            return Ok(privilegios);
        }

        // PUT: api/Privilegios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPrivilegios(int id, Privilegios privilegios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != privilegios.IdPrivilegio)
            {
                return BadRequest();
            }

            db.Entry(privilegios).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrivilegiosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Privilegios
        [ResponseType(typeof(Privilegios))]
        public async Task<IHttpActionResult> PostPrivilegios(Privilegios privilegios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Privilegio.Add(privilegios);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = privilegios.IdPrivilegio }, privilegios);
        }

        // DELETE: api/Privilegios/5
        [ResponseType(typeof(Privilegios))]
        public async Task<IHttpActionResult> DeletePrivilegios(int id)
        {
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            if (privilegios == null)
            {
                return NotFound();
            }

            db.Privilegio.Remove(privilegios);
            await db.SaveChangesAsync();

            return Ok(privilegios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PrivilegiosExists(int id)
        {
            return db.Privilegio.Count(e => e.IdPrivilegio == id) > 0;
        }
    }
}