﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers.api
{
    public class ServiciosController : ApiController
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: api/Servicios
        public IQueryable<Servicios> GetServicio()
        {
            return db.Servicio;
        }

        // GET: api/Servicios/5
        [ResponseType(typeof(Servicios))]
        public async Task<IHttpActionResult> GetServicios(int id)
        {
            Servicios servicios = await db.Servicio.FindAsync(id);
            if (servicios == null)
            {
                return NotFound();
            }

            return Ok(servicios);
        }

        // PUT: api/Servicios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutServicios(int id, Servicios servicios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != servicios.IdServicio)
            {
                return BadRequest();
            }

            db.Entry(servicios).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiciosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Servicios
        [ResponseType(typeof(Servicios))]
        public async Task<IHttpActionResult> PostServicios(Servicios servicios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Servicio.Add(servicios);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = servicios.IdServicio }, servicios);
        }

        // DELETE: api/Servicios/5
        [ResponseType(typeof(Servicios))]
        public async Task<IHttpActionResult> DeleteServicios(int id)
        {
            Servicios servicios = await db.Servicio.FindAsync(id);
            if (servicios == null)
            {
                return NotFound();
            }

            db.Servicio.Remove(servicios);
            await db.SaveChangesAsync();

            return Ok(servicios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServiciosExists(int id)
        {
            return db.Servicio.Count(e => e.IdServicio == id) > 0;
        }
    }
}