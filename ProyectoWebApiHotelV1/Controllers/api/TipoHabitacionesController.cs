﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers.api
{
    public class TipoHabitacionesController : ApiController
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: api/TipoHabitaciones
        public IQueryable<TipoHabitaciones> GetTipoHabitacion()
        {
            return db.TipoHabitacion;
        }

        // GET: api/TipoHabitaciones/5
        [ResponseType(typeof(TipoHabitaciones))]
        public async Task<IHttpActionResult> GetTipoHabitaciones(int id)
        {
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            if (tipoHabitaciones == null)
            {
                return NotFound();
            }

            return Ok(tipoHabitaciones);
        }

        // PUT: api/TipoHabitaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoHabitaciones(int id, TipoHabitaciones tipoHabitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoHabitaciones.IdTipoHabitacion)
            {
                return BadRequest();
            }

            db.Entry(tipoHabitaciones).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoHabitacionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoHabitaciones
        [ResponseType(typeof(TipoHabitaciones))]
        public async Task<IHttpActionResult> PostTipoHabitaciones(TipoHabitaciones tipoHabitaciones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoHabitacion.Add(tipoHabitaciones);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoHabitaciones.IdTipoHabitacion }, tipoHabitaciones);
        }

        // DELETE: api/TipoHabitaciones/5
        [ResponseType(typeof(TipoHabitaciones))]
        public async Task<IHttpActionResult> DeleteTipoHabitaciones(int id)
        {
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            if (tipoHabitaciones == null)
            {
                return NotFound();
            }

            db.TipoHabitacion.Remove(tipoHabitaciones);
            await db.SaveChangesAsync();

            return Ok(tipoHabitaciones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoHabitacionesExists(int id)
        {
            return db.TipoHabitacion.Count(e => e.IdTipoHabitacion == id) > 0;
        }
    }
}