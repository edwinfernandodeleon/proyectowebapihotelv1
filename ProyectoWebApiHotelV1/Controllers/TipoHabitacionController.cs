﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class TipoHabitacionController : Controller
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: TipoHabitacion
        public async Task<ActionResult> Index()
        {
            return View(await db.TipoHabitacion.ToListAsync());
        }

        // GET: TipoHabitacion/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            if (tipoHabitaciones == null)
            {
                return HttpNotFound();
            }
            return View(tipoHabitaciones);
        }

        // GET: TipoHabitacion/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoHabitacion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdTipoHabitacion,Tipo,Precio")] TipoHabitaciones tipoHabitaciones)
        {
            if (ModelState.IsValid)
            {
                db.TipoHabitacion.Add(tipoHabitaciones);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tipoHabitaciones);
        }

        // GET: TipoHabitacion/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            if (tipoHabitaciones == null)
            {
                return HttpNotFound();
            }
            return View(tipoHabitaciones);
        }

        // POST: TipoHabitacion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdTipoHabitacion,Tipo,Precio")] TipoHabitaciones tipoHabitaciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoHabitaciones).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipoHabitaciones);
        }

        // GET: TipoHabitacion/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            if (tipoHabitaciones == null)
            {
                return HttpNotFound();
            }
            return View(tipoHabitaciones);
        }

        // POST: TipoHabitacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TipoHabitaciones tipoHabitaciones = await db.TipoHabitacion.FindAsync(id);
            db.TipoHabitacion.Remove(tipoHabitaciones);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
