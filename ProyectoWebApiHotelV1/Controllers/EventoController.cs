﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class EventoController : Controller
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: Evento
        public async Task<ActionResult> Index()
        {
            var evento = db.Evento.Include(e => e.Hotel);
            return View(await evento.ToListAsync());
        }

        // GET: Evento/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eventos eventos = await db.Evento.FindAsync(id);
            if (eventos == null)
            {
                return HttpNotFound();
            }
            return View(eventos);
        }

        // GET: Evento/Create
        public ActionResult Create()
        {
            ViewBag.IdHotel = new SelectList(db.Hotel, "IdHotel", "Nombre");
            return View();
        }

        // POST: Evento/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdEvento,NombreEvento,FechaEvento,IdHotel")] Eventos eventos)
        {
            if (ModelState.IsValid)
            {
                db.Evento.Add(eventos);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IdHotel = new SelectList(db.Hotel, "IdHotel", "Nombre", eventos.IdHotel);
            return View(eventos);
        }

        // GET: Evento/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eventos eventos = await db.Evento.FindAsync(id);
            if (eventos == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdHotel = new SelectList(db.Hotel, "IdHotel", "Nombre", eventos.IdHotel);
            return View(eventos);
        }

        // POST: Evento/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdEvento,NombreEvento,FechaEvento,IdHotel")] Eventos eventos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eventos).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdHotel = new SelectList(db.Hotel, "IdHotel", "Nombre", eventos.IdHotel);
            return View(eventos);
        }

        // GET: Evento/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eventos eventos = await db.Evento.FindAsync(id);
            if (eventos == null)
            {
                return HttpNotFound();
            }
            return View(eventos);
        }

        // POST: Evento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Eventos eventos = await db.Evento.FindAsync(id);
            db.Evento.Remove(eventos);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
