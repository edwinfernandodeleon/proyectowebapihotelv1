﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoWebApiHotelV1.Models;

namespace ProyectoWebApiHotelV1.Controllers
{
    public class PrivilegioController : Controller
    {
        private ApiHotelContexto db = new ApiHotelContexto();

        // GET: Privilegio
        public async Task<ActionResult> Index()
        {
            return View(await db.Privilegio.ToListAsync());
        }

        // GET: Privilegio/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // GET: Privilegio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Privilegio/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdPrivilegio,Privilegio")] Privilegios privilegios)
        {
            if (ModelState.IsValid)
            {
                db.Privilegio.Add(privilegios);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(privilegios);
        }

        // GET: Privilegio/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // POST: Privilegio/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdPrivilegio,Privilegio")] Privilegios privilegios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(privilegios).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(privilegios);
        }

        // GET: Privilegio/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            if (privilegios == null)
            {
                return HttpNotFound();
            }
            return View(privilegios);
        }

        // POST: Privilegio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Privilegios privilegios = await db.Privilegio.FindAsync(id);
            db.Privilegio.Remove(privilegios);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
